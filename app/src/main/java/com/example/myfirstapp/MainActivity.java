package com.example.myfirstapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;

public class MainActivity extends Activity {
    static WebView webView;
    private Bundle savedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        init();
    }

    private void init() {
        setContentView(R.layout.activity_main);
        webView = (WebView) findViewById(R.id.webview);

        CookieSyncManager.createInstance(this);

        setWebViewSettings();

        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        } else {
            webView.loadUrl("https://wasm-testing.sqlite.org/speedtest1-worker.html?vfs=opfs-sahpool&size=10");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(webView == null) {
            init();
        }

        webView.onResume();
        CookieSyncManager.getInstance().startSync();
        CookieSyncManager.getInstance().sync();
    }

    @Override
    protected void onPause() {
        super.onPause();
        webView.onPause();
        CookieSyncManager.getInstance().sync();
        CookieSyncManager.getInstance().stopSync();
    }

    @Override
    protected void onDestroy() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebViewSettings() {
        WebView.setWebContentsDebuggingEnabled(true);
        WebSettings s = webView.getSettings();
        s.setJavaScriptEnabled(true);
        s.setDomStorageEnabled(true);
        s.setDatabaseEnabled(true);
        s.setLoadsImagesAutomatically(true);
        s.setBuiltInZoomControls(false);
        s.setTextZoom(100);
        s.setAllowFileAccess(true);
        s.setAllowFileAccessFromFileURLs(true);
        // Load with NO_CACHE as we are using appCache which is an extra layer over HTTP caching.
        s.setCacheMode(WebSettings.LOAD_NO_CACHE);

        s.setLoadWithOverviewMode(true);
        s.setUseWideViewPort(true);
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
    }
}
